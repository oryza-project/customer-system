import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public formcustpers : boolean;
  public formcustorgs : boolean;
  public items: MenuItem[];
  filterButtons = [
    { icon: "pi pi-home", name: "Dashboard", isClicked: true, route:'/home' },
    { icon: "pi pi-user", name: "Customer List", isClicked: false, route:'/customer-list'},
  ]

  constructor() { }

  ngOnInit(): void {
    this.items = [
      {label: 'New', icon: 'pi pi-fw pi-plus'},
      {label: 'Open', icon: 'pi pi-fw pi-download'},
      {label: 'Undo', icon: 'pi pi-fw pi-refresh'}
    ];

    //this.filterButtons[0].isClicked = true;
    for(let but of this.filterButtons) {
      but.isClicked = false;
      if(but.route==localStorage.getItem('route')){
        but.isClicked = true;
      }
    }
  }

  setActive(button: any): void {
    for(let but of this.filterButtons) {
      but.isClicked = false;
    }
    localStorage.setItem('route', button.route);
    button.isClicked = true;
  }

}
