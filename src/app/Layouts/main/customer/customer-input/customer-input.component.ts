import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Customer } from 'src/domain/customer';
import { CustomerService } from 'src/assets/helper/endpoint';

interface Type {
  name: string,
  value: string
}

@Component({
  selector: 'app-customer-input',
  templateUrl: './customer-input.component.html',
  styleUrls: ['./customer-input.component.css'],
  providers: [ConfirmationService, MessageService],
})
export class CustomerInputComponent implements OnInit {

  submitted = false;
  @Output() resultData = new EventEmitter<any>()
  @Input() dataList: any

  formCreateCustomer = this._fb.group({
    code: [''],
    name: ['', Validators.required],
    address: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', Validators.email],
    city: ['', Validators.required],
    description: ['', Validators.required],
  });

  constructor(
    private _fb: FormBuilder,
    private router: Router,
    private customerService: CustomerService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  customerData: Customer;

  customerList: any
  dataDetail: any;
  modeEdit: boolean = false;
  groupList: Type[];

  ngOnInit(): void {
    this.customerList = JSON.parse(sessionStorage.getItem("customerList"))
    this.dataDetail = JSON.parse(sessionStorage.getItem("dataEdit"))

    this.groupList = [
      { name: 'Marketing', value: 'Marketing' },
      { name: 'Services', value: 'Services' },
      { name: 'Human Resources', value: 'Human Resources' },
      { name: 'Accounting', value: 'Accounting' },
      { name: 'Business Development', value: 'Business Development' },
      { name: 'Sales', value: 'Sales' },
      { name: 'Training', value: 'Training' },
      { name: 'Legal', value: 'Legal' },
      { name: 'Support', value: 'Support' },
      { name: 'Product Management', value: 'Product Management' },
    ];

    if (this.dataDetail != null) {
      this.formCreateCustomer.patchValue(this.dataDetail);
    }
  }

  ngOnDestroy() {
    sessionStorage.removeItem("dataEdit")
  }

  get f() {
    return this.formCreateCustomer.controls;
  }

  cancel() {
    this.router.navigate(['customer-list']);
  }

  valueGroup(e: any) {
    this.formCreateCustomer.get('group').setValue(e.value.name);
  }

  onSubmit() {
    function getDateString() {
      const date = new Date();
      const year = date.getFullYear();
      const month = `${date.getMonth() + 1}`.padStart(2, '0');
      const day = `${date.getDate()}`.padStart(2, '0');
      return `${day}/${month}/${year}`
    }
    function time() {
      var d = new Date();
      var s = d.getSeconds();
      var m = d.getMinutes();
      var h = d.getHours();
      return `${h}:${m}:${s}`;
    }
    if (!this.formCreateCustomer.valid) {
      this.submitted = true
      return;
    } else {
      this.confirmationService.confirm({
        message: 'Are you sure that you want to proceed?',
        header: 'Confirmation',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          let data = this.dataList
          this.dataList = {
            code: this.formCreateCustomer.get('code').value,
            name: this.formCreateCustomer.get('name').value,
            address: this.formCreateCustomer.get('address').value,
            phone: this.formCreateCustomer.get('phone').value,
            email: this.formCreateCustomer.get('email').value,
            city: this.formCreateCustomer.get('city').value,
            date: getDateString(),
            time: time(),
            description: this.formCreateCustomer.get('description').value,
          }
          console.log(this.dataList)
          // this.customerList.push(this.customerData)
          this.router.navigate(['customer-list']);
          this.resultData.emit(this.dataList)
          sessionStorage.setItem("customerCreate", JSON.stringify(this.dataList))
          console.log(JSON.parse(sessionStorage.getItem("customerCreate")))
          JSON.parse(sessionStorage.getItem("customerList")).push(this.dataList)
          console.log(JSON.parse(sessionStorage.getItem("customerList")))
          this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Message Content' });
        },
        reject: () => {
          this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
        }
      })
    }
  }

  onEdit() {
    this.modeEdit = true;
  }

  onUpdate() {

  }

}
