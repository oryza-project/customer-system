import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FormBuilder, Validators } from '@angular/forms';
import { CustomerService } from 'src/assets/helper/endpoint';
import { Router } from '@angular/router';
import { Customer } from 'src/domain/customer';

interface Option {
  name: string;
}

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css'],
  providers: [ConfirmationService, MessageService],
})
export class CustomerListComponent implements OnInit {

  customer: any;
  dialogTicket: boolean = false

  constructor(
    private customerService: CustomerService,
    private _fb: FormBuilder,
    private router: Router,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit() {
    this.getCustomer()
  }

  getCustomer() {
    this.customerService.getCustomer().then((data) => {
      this.customer = data;
      this.customer.push(JSON.parse(sessionStorage.getItem("customerCreate")))
      sessionStorage.setItem("customerList", JSON.stringify(this.customer))
      this.customer =[...this.customer]
      console.log(this.customer)
    });
  }

  showFormCreateCustomer() {
    this.router.navigate(['customer-input']);
  }

  dataUpdate: any

  editData(item) {
    this.dataUpdate = item
    sessionStorage.setItem("dataEdit", JSON.stringify(this.dataUpdate))
    this.router.navigate(['customer-input']);
    console.log(item)
  }

  deleteData(index, item) {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to proceed?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        var index = this.customer.indexOf(item);
        if (index >= 0) {
          this.customer.splice(index, 1);
          console.log(this.customer)
          this.customer = this.customer
        }
        this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Message Content' });
      },
      reject: () => {
        this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Message Content' });
      }
    })
  }

  codeCustomer: any;
  timeCustomer: any;
  dateCustomer: any;

  showModal(data) {
    this.codeCustomer = data.code
    this.dateCustomer = data.date
    this.timeCustomer = data.time
    this.dialogTicket = true
  }

  closeModal() {
    this.dialogTicket = false
  }

  printTicket() {

  }

}
