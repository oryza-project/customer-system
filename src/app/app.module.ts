import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { InputNumberModule } from 'primeng/inputnumber';
import { MenuModule } from 'primeng/menu';
import { TableModule } from 'primeng/table';
import { CardModule } from 'primeng/card';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { DropdownModule } from 'primeng/dropdown';
import { InputSwitchModule } from 'primeng/inputswitch';
import { RadioButtonModule } from 'primeng/radiobutton';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { CheckboxModule } from 'primeng/checkbox';
import { FileUploadModule } from 'primeng/fileupload';
import { TooltipModule } from 'primeng/tooltip';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DialogModule } from 'primeng/dialog';
import { StepsModule } from 'primeng/steps';
import { ToastModule } from 'primeng/toast';
import { PanelModule } from 'primeng/panel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { PaginatorModule } from 'primeng/paginator';
import { CalendarModule } from 'primeng/calendar';

import { HeaderComponent } from './Layouts/header/header.component';
import { SidebarComponent } from './Layouts/sidebar/sidebar.component';
import { CustomerListComponent } from './Layouts/main/customer/customer-list.component';
import { HomeComponent } from './Layouts/main/home/home.component';
import { CustomerService } from 'src/assets/helper/endpoint';
import { CustomerInputComponent } from './Layouts/main/customer/customer-input/customer-input.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    CustomerListComponent,
    HomeComponent,
    CustomerInputComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ButtonModule,
    InputTextModule,
    InputNumberModule,
    MenuModule,
    TableModule,
    CardModule,
    BreadcrumbModule,
    DropdownModule,
    InputSwitchModule,
    RadioButtonModule,
    InputTextareaModule,
    CalendarModule,
    FormsModule,
    ReactiveFormsModule,
    CheckboxModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule,
    MessagesModule,
    MessageModule,
    ConfirmDialogModule,
    DialogModule,
    StepsModule,
    ToastModule,
    PanelModule,
    SelectButtonModule,
    PaginatorModule,
  ],
  providers: [CustomerService],
  bootstrap: [AppComponent],
})
export class AppModule { }
