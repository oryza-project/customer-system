import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './Layouts/main/home/home.component';
import { PageNotFoundComponent } from './Layouts/main/page-not-found/page-not-found.component';
import { CustomerListComponent } from './Layouts/main/customer/customer-list.component';
import { CustomerInputComponent } from './Layouts/main/customer/customer-input/customer-input.component';
import { HeaderComponent } from './Layouts/header/header.component';
import { SidebarComponent } from './Layouts/sidebar/sidebar.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'customer-list', component: CustomerListComponent },
  { path: 'customer-input', component: CustomerInputComponent },
  { path: 'main', component: HeaderComponent },
  { path: 'sidebar', component: SidebarComponent },
  { path: '**', component: PageNotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
export const routingComponent = [
  HomeComponent,
  CustomerListComponent,
  PageNotFoundComponent,
  CustomerInputComponent
];
