export interface Customer {
    code?: string;
    name?: string;
    address?: string;
    phone?: string;
    email?: string;
    city?: string;
    description?: string;   
}